package ar.edu.undec.adapter.data.Customer.exception;

public class CustomerMappingException extends RuntimeException {
    public CustomerMappingException(String message) {
        super (message);
    }
}
