package ar.edu.undec.adapter.data.Customer.crud;

import ar.edu.undec.adapter.data.Customer.model.CustomerEntity;
import customer.interactor.CreateCustomerRequestModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CreateCustomerCRUD extends CrudRepository<CustomerEntity, Integer> {
    boolean existsByDNI(String dni);

  //  Integer saveCustomer(CreateCustomerRequestModel customerRequestModel);
}
