package ar.edu.undec.adapter.data.Customer.mapper;

import ar.edu.undec.adapter.data.Customer.exception.CustomerMappingException;
import ar.edu.undec.adapter.data.Customer.model.CustomerEntity;
import customer.interactor.CreateCustomerRequestModel;
import customer.model.Customer;

public class CustomerDataMapper {

    public static Customer dataCoreMapper(CustomerEntity customerEntity){

        try{
            return Customer.instance(customerEntity.getId(), customerEntity.getFirstName(),
                    customerEntity.getLastName(), customerEntity.getDni(), customerEntity.getAddress(),
                    customerEntity.getEmail(), customerEntity.getCellphone(), customerEntity.getDayOfBirth());
        }catch (Exception e){
            throw new CustomerMappingException("Error on mapping Customer to Core");
        }
    }
    public static CustomerEntity dataEntityMapper(Customer customer){

        try{
            return new CustomerEntity(customer.getID(), customer.getFirstName(),
                    customer.getLastName(), customer.getDni(),
                    customer.getAddress(), customer.getEmail(),
                    customer.getCellphone(), customer.getDayOfBirth() );
        }catch (Exception e){
            throw new CustomerMappingException("Error on mapping Customer to Entity");
        }
    }

}
