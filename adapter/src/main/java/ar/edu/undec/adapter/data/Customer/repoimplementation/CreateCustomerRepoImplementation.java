package ar.edu.undec.adapter.data.Customer.repoimplementation;

import ar.edu.undec.adapter.data.Customer.crud.CreateCustomerCRUD;
import ar.edu.undec.adapter.data.Customer.mapper.CustomerDataMapper;
import customer.interactor.CreateCustomerRequestModel;
import customer.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import customer.output.CreateCustomerRepository;

@Service
public class CreateCustomerRepoImplementation implements CreateCustomerRepository{

    private CreateCustomerCRUD createCustomerCRUD;

    @Autowired
    public CreateCustomerRepoImplementation(CreateCustomerCRUD createCustomerCRUD) {
        this.createCustomerCRUD = createCustomerCRUD;
    }

    @Override
    public boolean existsByDNI(String dni) {
        return createCustomerCRUD.existsByDNI(dni);
    }

    @Override
    public Integer saveCustomer(Customer customer) {
        try{
            return createCustomerCRUD.save(CustomerDataMapper.dataEntityMapper(customer)).getId();
        }catch (Exception e){
            return 0;
        }
    }
}
