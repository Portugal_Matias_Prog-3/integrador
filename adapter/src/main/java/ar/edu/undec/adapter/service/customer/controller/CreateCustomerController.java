package ar.edu.undec.adapter.service.customer.controller;

import customer.exceptions.CustomerAlreadyExistException;
import customer.input.CreateCustomerInput;
import customer.interactor.CreateCustomerRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")

public class CreateCustomerController {

    private CreateCustomerInput createCustomerInput;
    @Autowired
    public CreateCustomerController(CreateCustomerInput createCustomerInput) {
        this.createCustomerInput = createCustomerInput;
    }

    @PostMapping
    public ResponseEntity<?> createCustomer (@RequestBody CreateCustomerRequestModel createCustomerRequestModel){
        try{
            return ResponseEntity.created(null).body(createCustomerInput.createCustomer(createCustomerRequestModel));
        }catch (CustomerAlreadyExistException e){
            return ResponseEntity.badRequest().build();
        }
    }
}
