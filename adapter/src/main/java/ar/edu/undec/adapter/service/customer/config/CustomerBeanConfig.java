package ar.edu.undec.adapter.service.customer.config;

import customer.interactor.CreateCustomerUseCase;
import customer.interactor.UpdateCustomerUseCase;
import customer.output.CreateCustomerRepository;
import customer.output.UpdateCustomerRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerBeanConfig {

    @Bean
    public CreateCustomerUseCase createCustomerUseCase(CreateCustomerRepository createCustomerRepository) {

        return new CreateCustomerUseCase(createCustomerRepository);

    }

}
