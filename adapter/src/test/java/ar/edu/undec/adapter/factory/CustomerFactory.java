package ar.edu.undec.adapter.factory;

import ar.edu.undec.adapter.data.Customer.model.CustomerEntity;
import customer.interactor.CreateCustomerRequestModel;
import customer.model.Customer;

import java.time.LocalDate;

public class CustomerFactory {
    public CustomerFactory() {
    }
    public static CustomerEntity returnCustomerEntity(){
        return new CustomerEntity(1, "Matias", "Portugal", "3838", "Juan XXIII", "mnp@mail.com",
                "3838", LocalDate.of(1995, 6, 9));
    }
    public static Customer customerInstance(){
        return Customer.instance(null, "Matias", "Portugal", "3838", "Juan XXIII", "mnp@mail.com",
                "3838", LocalDate.of(1995, 6, 9) );
    }
    public static CreateCustomerRequestModel customerRequestModel(){
        return CreateCustomerRequestModel.instance("Matias", "Portugal", "3838", "Juan XXIII", "mnp@mail.com",
                "3838", LocalDate.of(1995, 6, 9));
    }
}
