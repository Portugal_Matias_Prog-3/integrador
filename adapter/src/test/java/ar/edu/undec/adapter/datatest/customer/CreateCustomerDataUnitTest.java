package ar.edu.undec.adapter.datatest.customer;

import ar.edu.undec.adapter.data.Customer.crud.CreateCustomerCRUD;
import ar.edu.undec.adapter.data.Customer.model.CustomerEntity;
import ar.edu.undec.adapter.data.Customer.repoimplementation.CreateCustomerRepoImplementation;
import ar.edu.undec.adapter.factory.CustomerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CreateCustomerDataUnitTest {
    @InjectMocks
    CreateCustomerRepoImplementation createCustomerRepoImplementation;

    @Mock
    CreateCustomerCRUD createCustomerCRUD;

    @Test
    void saveCustomer_CustomerSaved_ReturnNewId(){
        when (createCustomerCRUD.save(any(CustomerEntity.class))).thenReturn(
                (CustomerFactory.returnCustomerEntity()));
        Integer dataBaseResponse = createCustomerRepoImplementation.saveCustomer(
                CustomerFactory.customerInstance());
        Assertions.assertEquals(1, dataBaseResponse);

    }
    @Test
    void saveCustomer_DataBaseException_Return0(){
        doThrow(RuntimeException.class).when(createCustomerCRUD).save(any(CustomerEntity.class));
        Integer dataBaseResponse = createCustomerRepoImplementation.saveCustomer(
                CustomerFactory.customerInstance());

        Assertions.assertEquals(0, dataBaseResponse);
    }
}
