package ar.edu.undec.adapter.servicetest.customer;

import ar.edu.undec.adapter.factory.CustomerFactory;
import ar.edu.undec.adapter.service.customer.controller.CreateCustomerController;
import customer.exceptions.CustomerAlreadyExistException;
import customer.input.CreateCustomerInput;
import customer.interactor.CreateCustomerRequestModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)

public class CreateCustomerControllerUnitTest {
    @InjectMocks
    CreateCustomerController createCustomerController;

    @Mock
    CreateCustomerInput createCustomerInput;

    @Test
    void createCustomer_CustomerCreated_ReturnHTTP201(){
        when(createCustomerInput.createCustomer(any(CreateCustomerRequestModel.class))).thenReturn(1);
        ResponseEntity<?> createCustomerResponse = createCustomerController.createCustomer(
                CustomerFactory.customerRequestModel());
        Assertions.assertEquals(HttpStatus.CREATED, createCustomerResponse.getStatusCode());
        Assertions.assertEquals(1,createCustomerResponse.getBody());
    }
    @Test
    void createCustomer_CustomerAlreadyExistException_ReturnHTTP400(){
        doThrow(CustomerAlreadyExistException.class).when(createCustomerInput).createCustomer(
                any(CreateCustomerRequestModel.class));
        ResponseEntity<?> createCustomerResponse = createCustomerController.createCustomer(
                CustomerFactory.customerRequestModel());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, createCustomerResponse.getStatusCode());

    }


}
