# Proyecto Integrador 2022

## Nombre
chop_chop-backend

## Descripcion
Proyecto integrador de la materia Programación 3 - 2022.
Se trata de un sistema de pedidos donde un cliente elije lo que quiere comprar y luego ese pedido es recibido por el local el cual lo prepara y lo despacha con el delivery

## Stack utilizado
Maven, Java, Spring Boot.

## Arquitectura
Clean Architecture - Principios SOLID - Patrones de Diseño







