package burger.output;

import burger.model.Burger;

public interface CreateBurgerRepository {
    boolean existsByName(String name);

    Integer saveBurger(Burger burger);
}
