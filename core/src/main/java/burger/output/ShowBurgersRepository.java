package burger.output;

import burger.model.Burger;

import java.util.Collection;

public interface ShowBurgersRepository {
    boolean burgersListIsEmpty();
    Collection<Burger> burgersList();
}
