package burger.output;

import burger.model.Burger;

public interface UpdateBurgerRepository {
    Burger getUnmodifiedBurger(String nameOfBurger);
}
