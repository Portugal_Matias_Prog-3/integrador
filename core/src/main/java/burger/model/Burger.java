package burger.model;

import burger.exceptions.BurgerIncompleteException;

public class Burger {
    private  Integer id;
    private  String name;
    private  Double price;
    private  String description;
    private  Integer stock;
    public static final Double MIN_PRICE = 350.0;

    private Burger(Integer id, String name, double price, String description, Integer stock) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.stock = stock;

    }

    public static Burger instance(Integer id, String name, Double price, String description, Integer stock) {
        if (name == null || name.isEmpty()){
            throw new BurgerIncompleteException("All burgers must have a name");
        }
        if (price < Burger.MIN_PRICE ){
            throw new BurgerIncompleteException("Burger price must be over: $" + Burger.MIN_PRICE);
        }
        if (stock < 0){
            throw new BurgerIncompleteException("Stock field can't be a negative value");
        }

        return new Burger(id, name, price, description, stock);
    }

    public String getCompleteInfo() {
        return ("id: " + id + " - Name: " + name + " - Price:$ " + price +
                " - Description: " + description + " minimum price:$ " + Burger.MIN_PRICE +
                " - Stock Amount" + stock);
    }

    public String getNameOfBurger() {
        return this.name;
    }

    public double getPriceOfBurger() {
        return this.price;
    }

    public String getDescriptionOfBurger() {
        return this.description;
    }
}
