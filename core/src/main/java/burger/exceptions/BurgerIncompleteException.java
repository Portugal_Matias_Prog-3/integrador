package burger.exceptions;

public class BurgerIncompleteException extends RuntimeException{
    public BurgerIncompleteException(String message) {
        super(message);
    }
}
