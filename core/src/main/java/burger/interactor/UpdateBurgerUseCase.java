package burger.interactor;

import burger.exceptions.BurgerFieldChangeException;
import burger.input.UpdateBurgerInput;
import burger.output.UpdateBurgerRepository;

public class UpdateBurgerUseCase implements UpdateBurgerInput {
    private UpdateBurgerRepository updateBurgerRepository;

    public UpdateBurgerUseCase(UpdateBurgerRepository updateBurgerRepository) {
        this.updateBurgerRepository = updateBurgerRepository;
    }

    @Override
    public boolean setNewPrice(String nameOfBurger, Double price) {

        if(updateBurgerRepository.getUnmodifiedBurger(nameOfBurger).getPriceOfBurger() == price){
            throw new BurgerFieldChangeException(String.format("$%a already is %s´s price", price, nameOfBurger));
        }

        return true;
    }

    @Override
    public boolean setNewDescription(String nameOfBurger, String description) {
        if(updateBurgerRepository.getUnmodifiedBurger(nameOfBurger).getDescriptionOfBurger().equals(description)){
            throw new BurgerFieldChangeException("No changes have been made in the description");
        }

        return true;
    }
}
