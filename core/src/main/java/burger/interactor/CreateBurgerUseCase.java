package burger.interactor;

import burger.exceptions.BurgerAlreadyExistsException;
import burger.input.CreateBurgerInput;
import burger.model.Burger;
import burger.output.CreateBurgerRepository;

public class CreateBurgerUseCase implements CreateBurgerInput {
    private CreateBurgerRepository createBurgerRepository;

    public CreateBurgerUseCase(CreateBurgerRepository createBurgerRepository) {
        this.createBurgerRepository = createBurgerRepository;
    }

    @Override
    public int createBurger(Burger myBurger) {
        if(createBurgerRepository.existsByName(myBurger.getNameOfBurger())){
            throw new BurgerAlreadyExistsException(String.format("Burger %s already exists!",
                    myBurger.getNameOfBurger()));
        }
        Integer newID = createBurgerRepository.saveBurger(myBurger);
        return newID;
    }
}
