package burger.interactor;

import burger.exceptions.BurgersListEmptyException;
import burger.input.ShowBurgersInput;
import burger.model.Burger;
import burger.output.ShowBurgersRepository;

import java.util.Collection;

public class ShowBurgersUseCase implements ShowBurgersInput {
    private ShowBurgersRepository showBurgersRepository;

    public ShowBurgersUseCase(ShowBurgersRepository showBurgersRepository) {
        this.showBurgersRepository = showBurgersRepository;
    }

    @Override
    public Collection<Burger> showBurgers() {
        if(showBurgersRepository.burgersListIsEmpty()){
            throw new BurgersListEmptyException("There are no products uploaded yet");
        }
        return showBurgersRepository.burgersList();
    }
}
