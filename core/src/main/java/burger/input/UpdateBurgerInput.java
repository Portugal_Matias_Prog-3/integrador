package burger.input;

public interface UpdateBurgerInput {

    boolean setNewPrice(String nameOfBurger, Double price);
    boolean setNewDescription(String nameOfBurger, String description);
}
