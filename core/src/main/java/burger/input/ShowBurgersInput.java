package burger.input;

import burger.model.Burger;

import java.util.Collection;

public interface ShowBurgersInput {
    Collection<Burger> showBurgers();
}
