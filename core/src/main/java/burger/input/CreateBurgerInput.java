package burger.input;

import burger.model.Burger;

public interface CreateBurgerInput {
    int createBurger(Burger myBurger);
}
