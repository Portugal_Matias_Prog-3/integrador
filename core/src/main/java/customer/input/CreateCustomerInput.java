package customer.input;

import customer.interactor.CreateCustomerRequestModel;

public interface CreateCustomerInput {
    Integer createCustomer(CreateCustomerRequestModel customerRequestModel);
}
