package customer.input;

public interface UpdateCustomerInput {
    boolean setNewAddressOfCustomer(String dni, String address);
    boolean setNewCellphoneOfCustomer(String dni, String cellphone);
}
