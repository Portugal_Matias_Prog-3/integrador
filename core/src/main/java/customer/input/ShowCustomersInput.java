package customer.input;


import customer.model.Customer;

import java.util.Collection;

public interface ShowCustomersInput {
    Collection<Customer> showCustomers();
}
