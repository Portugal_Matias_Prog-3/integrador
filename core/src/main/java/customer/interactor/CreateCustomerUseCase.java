package customer.interactor;

import customer.exceptions.CustomerAlreadyExistException;
import customer.input.CreateCustomerInput;
import customer.model.Customer;
import customer.output.CreateCustomerRepository;

public class CreateCustomerUseCase implements CreateCustomerInput {
    private CreateCustomerRepository createCustomerRepository;

    public CreateCustomerUseCase(CreateCustomerRepository createCustomerRepository) {

        this.createCustomerRepository = createCustomerRepository;
    }


    @Override
    public Integer createCustomer(CreateCustomerRequestModel createCustomerRequestModel) {
        if (createCustomerRepository.existsByDNI(createCustomerRequestModel.getDni())) {
            throw new CustomerAlreadyExistException(String.format("Customer %s already exist with dni: %s",
                    createCustomerRequestModel.getFirstName(), createCustomerRequestModel.getDni()));
        }
        Customer myCustomer = Customer.instance(null, createCustomerRequestModel.getFirstName(),
                createCustomerRequestModel.getLastName(),createCustomerRequestModel.getDni(),
                createCustomerRequestModel.getAddress(),createCustomerRequestModel.getEmail(),
                createCustomerRequestModel.getCellphone(), createCustomerRequestModel.getDayOfBirth());
        return createCustomerRepository.saveCustomer(myCustomer);
    }
}
