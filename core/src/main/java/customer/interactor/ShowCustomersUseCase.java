package customer.interactor;

import burger.exceptions.BurgersListEmptyException;
import customer.exceptions.CustomersListEmptyException;
import customer.input.ShowCustomersInput;
import customer.model.Customer;
import customer.output.ShowCustomersRepository;

import java.util.Collection;

public class ShowCustomersUseCase implements ShowCustomersInput {
    private ShowCustomersRepository showCustomersRepository;

    public ShowCustomersUseCase(ShowCustomersRepository showCustomersRepository) {
        this.showCustomersRepository = showCustomersRepository;
    }

    @Override
    public Collection<Customer> showCustomers() {
        if(showCustomersRepository.customersListIsEmpty()){
            throw new CustomersListEmptyException("There are no products uploaded yet");
        }
        return showCustomersRepository.customersList();
    }
}
