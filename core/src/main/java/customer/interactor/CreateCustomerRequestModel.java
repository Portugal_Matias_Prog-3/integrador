package customer.interactor;

import java.time.LocalDate;

public class CreateCustomerRequestModel {

    private String firstName;
    private String lastName;
    private String dni;
    private String address;
    private String email;
    private String cellphone;
    private LocalDate dayOfBirth;

    private CreateCustomerRequestModel(String firstName, String lastName, String dni, String address, String email, String cellphone, LocalDate dayOfBirth){
        this.firstName = firstName;
        this.lastName = lastName;
        this.dni = dni;
        this.address = address;
        this.email = email;
        this.cellphone = cellphone;
        this.dayOfBirth = dayOfBirth;
    }
    private CreateCustomerRequestModel(){
    }
    public static CreateCustomerRequestModel instance(String firstName, String lastName, String dni, String address, String email, String cellphone, LocalDate dayOfBirth){
        try{
            CreateCustomerRequestModel createCustomerRequestModel = new CreateCustomerRequestModel(firstName,
                    lastName,dni, address, email, cellphone, dayOfBirth);
            return createCustomerRequestModel;
        }
        catch (Exception e){
            throw new RuntimeException("ERROR! can't create CreateCustomerRequestModel instance");
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDni() {
        return dni;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getCellphone() {
        return cellphone;
    }

    public LocalDate getDayOfBirth() {
        return dayOfBirth;
    }
}
