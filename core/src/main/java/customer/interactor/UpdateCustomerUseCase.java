package customer.interactor;

import customer.exceptions.CustomerFieldChangeException;
import customer.input.UpdateCustomerInput;
import customer.output.UpdateCustomerRepository;

public class UpdateCustomerUseCase implements UpdateCustomerInput {
    private UpdateCustomerRepository updateCustomerRepository;
    public UpdateCustomerUseCase(UpdateCustomerRepository updateCustomerRepository) {
        this.updateCustomerRepository = updateCustomerRepository;
    }

    @Override
    public boolean setNewAddressOfCustomer(String dni, String address){
        if (updateCustomerRepository.getUnmodifiedCustomer(dni).getAddress().equals(address)){
            throw new CustomerFieldChangeException("The address is the same");
        }
        return true;
    }

    @Override
    public boolean setNewCellphoneOfCustomer(String dni, String cellphone){
        if(updateCustomerRepository.getUnmodifiedCustomer(dni).getCellphone().equals(cellphone)){
            throw new CustomerFieldChangeException("The cell number is the same");
        }
        return true;
    }
}
