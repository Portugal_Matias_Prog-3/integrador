package customer.output;

import customer.model.Customer;

public interface UpdateCustomerRepository {
    Customer getUnmodifiedCustomer(String dni);
}
