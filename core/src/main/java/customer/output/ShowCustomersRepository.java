package customer.output;



import customer.model.Customer;

import java.util.Collection;

public interface ShowCustomersRepository {
    boolean customersListIsEmpty();
    Collection<Customer> customersList();
}
