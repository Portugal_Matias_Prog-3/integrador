package customer.output;

import customer.interactor.CreateCustomerRequestModel;
import customer.model.Customer;

public interface CreateCustomerRepository {
    boolean existsByDNI(String dni);

    Integer saveCustomer(Customer customer);

}
