package customer.exceptions;

public class CustomerIncompleteException extends RuntimeException {
    public CustomerIncompleteException(String message) {
        super(message);
    }
}
