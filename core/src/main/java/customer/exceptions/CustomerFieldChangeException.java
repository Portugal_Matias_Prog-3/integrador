package customer.exceptions;

public class CustomerFieldChangeException extends RuntimeException {
    public CustomerFieldChangeException(String message) {
        super(message);
    }
}
