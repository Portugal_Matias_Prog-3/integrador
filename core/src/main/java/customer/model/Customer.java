package customer.model;

import customer.exceptions.CustomerIncompleteException;

import java.time.LocalDate;

public class Customer {
    private Integer id;
    private String firstName;
    private String lastName;
    private String dni;
    private String address;
    private String email;
    private String cellphone;
    private LocalDate dayOfBirth;


    private Customer(Integer id, String firstName, String lastName, String dni, String address, String email, String cellphone, LocalDate dayOfBirth) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dni = dni;
        this.address = address;
        this.email = email;
        this.cellphone = cellphone;
        this.dayOfBirth = dayOfBirth;
    }

    public static Customer instance(Integer id, String firstName, String lastName, String dni,
                                    String address, String email, String cellphone, LocalDate dayOfBirth) {
        String age = dayOfBirth.toString();
        if(fieldValidation(firstName)){
            throw new CustomerIncompleteException("The first name field is required");
        }
        if(fieldValidation(lastName)){
            throw new CustomerIncompleteException("The last name field is required");
        }
        if(fieldValidation(dni)){
            throw new CustomerIncompleteException("The ID card field is required");
        }
        if(fieldValidation(email)){
            throw new CustomerIncompleteException("The email field is required");
        }
        if(fieldValidation(cellphone)){
            throw new CustomerIncompleteException("The cellphone field is required");
        }
        if(fieldValidation(age)){
            throw new CustomerIncompleteException("The field day of birth must is required");
        }
        if(ageValidation(dayOfBirth)){
            throw new CustomerIncompleteException("Must be over 18 years old");
        }

        return new Customer(id, firstName,lastName, dni, address, email, cellphone, dayOfBirth);
    }

    public String getCompleteInfo() {
        return (id + " - " + lastName + ", " + firstName + " - " + "DNI: " + dni + " - " +
                "Address: " + address + " - " + "email: " + email + " - " + "cell: " + cellphone + " - " +
                "born: " + dayOfBirth.toString());
    }
    public static boolean fieldValidation(String expected){

        if (expected == null || expected.isEmpty()) {
            return true;
        }
        return false;
    }
    private static boolean ageValidation(LocalDate dayOfBirthCheck) {
        boolean result = true;
        LocalDate actual = LocalDate.now();
        Integer age = actual.getYear() - dayOfBirthCheck.getYear();
        if(age == 18){
            if((actual.getMonthValue() - dayOfBirthCheck.getMonthValue()) >= 0){
                if((actual.getDayOfMonth() - dayOfBirthCheck.getDayOfMonth()) > 0){
                    result = true;
                }
                else{
                    result = false;
                    }
            }


        }
        else{
            result = (age<18 ? true: false);
        }
    return result;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getDni() {
        return this.dni;
    }

    public String getName() {
        return this.firstName;
    }

    public String getAddress() {
        return this.address;
    }

    public String getCellphone() {
        return this.cellphone;
    }

    public Integer getID() {
        return this.id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public LocalDate getDayOfBirth() {
        return this.dayOfBirth;
    }
}
