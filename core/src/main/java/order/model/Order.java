package order.model;

import burger.model.Burger;
import customer.model.Customer;
import order.exceptions.OrderIncompleteException;

import java.time.LocalDate;
import java.util.*;

public class Order {
    private Integer id = null;
    private Collection<Burger> myBurgers;
    private LocalDate orderDate;
    private String description;
    private Customer myCustomer;

    private Order(List<Burger> myBurgers, String description, Customer myCustomer) {
        Collections.sort(myBurgers, new Comparator<Burger>() {
            @Override
            public int compare(Burger b1, Burger b2) {
                return new String(b1.getNameOfBurger()).compareTo(new String(b2.getNameOfBurger()));
            }
        });
        this.myBurgers = myBurgers;
        this.orderDate = LocalDate.now();
        this.description = description;
        this.myCustomer = myCustomer;
    }

    public static Order instance(List<Burger> myBurgers, String description, Customer customer) {
        if(myBurgers.isEmpty()){
            throw new OrderIncompleteException("You must select at least one burger");
        }
        return new Order(myBurgers,description, customer);
    }

    public String getInfoOfBurgers() {
        Iterator<Burger> itr = this.myBurgers.iterator();
        //Burgers in the order are 2: Cheese Burger, Piggier!
        String result = "Burgers in the order are " + this.myBurgers.size() + ": ";
        while (itr.hasNext()){
            Burger burgers = itr.next();
            if(burgers.getNameOfBurger() != null ) {
                result += burgers.getNameOfBurger() + ", ";
            }
        }
        result = result.subSequence(0, result.length()-2) + "!";
        return result;
    }

}
