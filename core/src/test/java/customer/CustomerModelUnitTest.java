package customer;

import customer.exceptions.CustomerIncompleteException;
import customer.model.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class CustomerModelUnitTest {
    @Test
    void instance_AllAttributesAreCorrect_instanceOfCustomer(){
        //Arrange
        Integer id = null;
        String firstName =  "Matias";
        String lastName =  "Portugal";
        String dni =  "38480366";
        String address =  "Juan XXIII 229";
        String email =  "mnportu@gmail.com";
        String cellphone =  "3825-556677";
        LocalDate dayOfBirth = LocalDate.of(1995,6,9);

        //Act
        Customer myCustomer = Customer.instance(id,firstName, lastName,dni,address,email,cellphone,dayOfBirth);
        String myCustomerString = id + " - " + lastName + ", " + firstName + " - " + "DNI: " + dni + " - " +
                "Address: " + address + " - " + "email: " + email + " - " + "cell: " + cellphone + " - " +
                "born: " + dayOfBirth.toString();
        //Assert
        Assertions.assertNotNull(myCustomer);
        Assertions.assertTrue(myCustomerString.equals(myCustomer.getCompleteInfo()));
    }
    @Test
    void instance_firstNameIsMissing_CustomerIncompleteException(){
        //Arrange
        Integer id = null;

        String lastName =  "Portugal";
        String dni =  "38480366";
        String address =  "Juan XXIII 229";
        String email =  "mnportu@gmail.com";
        String cellphone =  "3825-556677";
        LocalDate dayOfBirth = LocalDate.of(1995,6,9);

        //Act
        Exception exceptionNull = Assertions.assertThrows(CustomerIncompleteException.class,
            ()-> Customer.instance(id, null, lastName,dni,address,email,cellphone,dayOfBirth));

        Exception exceptionEmpty = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, "", lastName,dni,address,email,cellphone,dayOfBirth));
        //Assert
        Assertions.assertEquals("The first name field is required", exceptionNull.getMessage());
        Assertions.assertEquals("The first name field is required", exceptionEmpty.getMessage());

    }
    @Test
    void instance_lastNameIsMissing_CustomerIncompleteException(){
        //Arrange
        Integer id = null;
        String firstName =  "Matias";

        String dni =  "38480366";
        String address =  "Juan XXIII 229";
        String email =  "mnportu@gmail.com";
        String cellphone =  "3825-556677";
        LocalDate dayOfBirth = LocalDate.of(1995,6,9);

        //Act
        Exception exceptionNull = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, firstName, null,dni,address,email,cellphone,dayOfBirth));

        Exception exceptionEmpty = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, firstName, "",dni,address,email,cellphone,dayOfBirth));
        //Assert
        Assertions.assertEquals("The last name field is required", exceptionNull.getMessage());
        Assertions.assertEquals("The last name field is required", exceptionEmpty.getMessage());

    }
    @Test
    void instance_dniIsMissing_CustomerIncompleteException(){
        //Arrange
        Integer id = null;
        String firstName =  "Matias";
        String lastName =  "Portugal";

        String address =  "Juan XXIII 229";
        String email =  "mnportu@gmail.com";
        String cellphone =  "3825-556677";
        LocalDate dayOfBirth = LocalDate.of(1995,6,9);

        //Act
        Exception exceptionNull = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, firstName, lastName,null,address,email,cellphone,dayOfBirth));

        Exception exceptionEmpty = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, firstName, lastName,"",address,email,cellphone,dayOfBirth));
        //Assert
        Assertions.assertEquals("The ID card field is required", exceptionNull.getMessage());
        Assertions.assertEquals("The ID card field is required", exceptionEmpty.getMessage());

    }
    @Test
    void instance_emailIsMissing_CustomerIncompleteException(){
        //Arrange
        Integer id = null;
        String firstName =  "Matias";
        String lastName =  "Portugal";
        String dni = "38480366";
        String address =  "Juan XXIII 229";

        String cellphone =  "3825-556677";
        LocalDate dayOfBirth = LocalDate.of(1995,6,9);

        //Act
        Exception exceptionNull = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, firstName, lastName,dni,address,null,cellphone,dayOfBirth));

        Exception exceptionEmpty = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, firstName, lastName,dni,address,"",cellphone,dayOfBirth));
        //Assert
        Assertions.assertEquals("The email field is required", exceptionNull.getMessage());
        Assertions.assertEquals("The email field is required", exceptionEmpty.getMessage());

    }
    @Test
    void instance_cellphoneIsMissing_CustomerIncompleteException(){
        //Arrange
        Integer id = null;
        String firstName =  "Matias";
        String lastName =  "Portugal";
        String dni = "38480366";
        String address =  "Juan XXIII 229";
        String email = "mnportu@gmail.com";

        LocalDate dayOfBirth = LocalDate.of(1995,6,9);

        //Act
        Exception exceptionNull = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, firstName, lastName,dni,address,email,null,dayOfBirth));

        Exception exceptionEmpty = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, firstName, lastName,dni,address,email,"",dayOfBirth));
        //Assert
        Assertions.assertEquals("The cellphone field is required", exceptionNull.getMessage());
        Assertions.assertEquals("The cellphone field is required", exceptionEmpty.getMessage());

    }
    @Test
    void instance_underAgeCustomer_CustomerIncompleteException(){
        //Arrange
        Integer id = null;
        String firstName =  "Matias";
        String lastName =  "Portugal";
        String dni = "38480366";
        String address =  "Juan XXIII 229";
        String email = "mnportu@gmail.com";
        String cellphone =  "3825-556677";
        LocalDate dayOfBirth = LocalDate.of(2004,4,5);

        //Act
        Exception exceptionUnderAge = Assertions.assertThrows(CustomerIncompleteException.class,
                ()-> Customer.instance(id, firstName, lastName,dni,address,email,cellphone,dayOfBirth));

        //Assert

        Assertions.assertEquals("Must be over 18 years old", exceptionUnderAge.getMessage());

    }
}
