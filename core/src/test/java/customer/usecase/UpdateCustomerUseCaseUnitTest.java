package customer.usecase;

import customer.input.UpdateCustomerInput;
import customer.interactor.UpdateCustomerUseCase;
import customer.model.Customer;
import customer.output.UpdateCustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UpdateCustomerUseCaseUnitTest {
    @Mock
    UpdateCustomerRepository updateCustomerRepository;

    @Test
    void updateCustomer_UpdateAddressOfCustomer_AddressUpdated(){
        Customer myCustomer1 = Customer.instance(null,"Matias", "Portugal",
                "38480366","Juan XXIII 229", "mnportu@gmail.com",
                "3838", LocalDate.of(1995,6,9));

        UpdateCustomerInput updateCustomerInteractor = new UpdateCustomerUseCase(updateCustomerRepository);

        when(updateCustomerRepository.getUnmodifiedCustomer("38480366")).thenReturn(myCustomer1);
        boolean expected = updateCustomerInteractor.setNewAddressOfCustomer(myCustomer1.getDni(),
                "Castro y Bazan 50");

        Assertions.assertTrue(expected);
    }
    @Test
    void updateCustomer_UpdateCellphoneOfCustomer_AddressUpdated() {
        Customer myCustomer1 = Customer.instance(null,"Matias", "Portugal",
                "38480366","Juan XXIII 229", "mnportu@gmail.com",
                "3838", LocalDate.of(1995,6,9));

        UpdateCustomerInput updateCustomerInteractor = new UpdateCustomerUseCase(updateCustomerRepository);

        when(updateCustomerRepository.getUnmodifiedCustomer("38480366")).thenReturn(myCustomer1);
        boolean expected = updateCustomerInteractor.setNewCellphoneOfCustomer(myCustomer1.getDni(),
                "3838");

        Assertions.assertTrue(expected);
    }
}
