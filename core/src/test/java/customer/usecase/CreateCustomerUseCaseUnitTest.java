package customer.usecase;

import customer.exceptions.CustomerAlreadyExistException;
import customer.input.CreateCustomerInput;
import customer.interactor.CreateCustomerRequestModel;
import customer.interactor.CreateCustomerUseCase;
import customer.model.Customer;
import customer.output.CreateCustomerRepository;
import factory.CustomerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateCustomerUseCaseUnitTest {
    @Mock
    CreateCustomerRepository createCustomerRepository;

    @Test
    void createCustomer_CustomerDoesNotExist_createsCustomer(){
        CreateCustomerRequestModel myCustomer = CustomerFactory.factoryCreateCustomerRequestModel();

        CreateCustomerInput createCustomerUseCase = new CreateCustomerUseCase(createCustomerRepository);
        when(createCustomerRepository.existsByDNI("38480366")).thenReturn(false);
        when(createCustomerRepository.saveCustomer(any(Customer.class))).thenReturn(1);

        Assertions.assertEquals(1,createCustomerUseCase.createCustomer(myCustomer));

    }
    @Test
    void createCustomer_CustomerExistByDNI_CustomerAlreadyExistsException(){
        CreateCustomerRequestModel myCustomer = CustomerFactory.factoryCreateCustomerRequestModel();

        verify(createCustomerRepository,never()).saveCustomer(any(Customer.class));
        CreateCustomerInput createCustomerUseCase = new CreateCustomerUseCase(createCustomerRepository);

        when(createCustomerRepository.existsByDNI("38480366")).thenReturn(true);

        Assertions.assertThrows(CustomerAlreadyExistException.class,
                ()->createCustomerUseCase.createCustomer(myCustomer));

    }


}
