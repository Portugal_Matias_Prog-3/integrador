package customer.usecase;


import burger.input.ShowBurgersInput;
import burger.interactor.ShowBurgersUseCase;
import burger.model.Burger;
import customer.input.ShowCustomersInput;
import customer.interactor.ShowCustomersUseCase;
import customer.model.Customer;
import customer.output.ShowCustomersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class ShowCustomersUseCaseUnitTest {
    @Mock
    ShowCustomersRepository showCustomersRepository;

    @Test
    void showCustomer_CustomersListIsNotEmpty_ShowsCustomers(){
        Collection<Customer> myCustomers = new ArrayList();
        Customer myCustomer1 = Customer.instance(null,"Matias", "Portugal",
                "38480366","Juan XXIII 229", "mnportu@gmail.com",
                "3838", LocalDate.of(1995,6,9));
        Customer myCustomer2 = Customer.instance(null,"Pepito", "Barcelona",
                "37000366","Juan XXIII 229", "pepito@gmail.com",
                "3939", LocalDate.of(1995,6,9));
        myCustomers.add(myCustomer1);
        myCustomers.add(myCustomer2);
        ShowCustomersInput showCustomersInteractor = new ShowCustomersUseCase(showCustomersRepository);

        when(showCustomersRepository.customersList()).thenReturn(myCustomers);

        Assertions.assertTrue(myCustomers.equals(showCustomersRepository.customersList()));

    }
    @Test
    void showCustomer_CustomersListIsEmpty_CustomersListEmptyException(){
        Collection<Customer> myCustomers = new ArrayList();

        ShowCustomersInput showCustomersInteractor = new ShowCustomersUseCase(showCustomersRepository);

        when(showCustomersRepository.customersListIsEmpty()).thenReturn(true);

        Assertions.assertTrue(showCustomersRepository.customersListIsEmpty());

    }

}
