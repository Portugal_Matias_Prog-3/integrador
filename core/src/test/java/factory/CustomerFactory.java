package factory;

import customer.interactor.CreateCustomerRequestModel;
import customer.model.Customer;

import java.time.LocalDate;

public class CustomerFactory {

    private CustomerFactory(){
    }
    public static Customer factoryCustomer(Integer id){
        return (Customer.instance(id, "Matias", "Portugal",
                "38480366", "Juan XXIII 229", "mnportu@gmail.com",
                "3838", LocalDate.of(1995,6,9)));
    }
    public static CreateCustomerRequestModel factoryCreateCustomerRequestModel(){
        CreateCustomerRequestModel createCustomerRequestModel = CreateCustomerRequestModel.instance("Matias",
                "Portugal","38480366", "Juan XXIII 229", "mnportu@gmail.com",
                "3838", LocalDate.of(1995,6,9));
        return createCustomerRequestModel;
    }
}
