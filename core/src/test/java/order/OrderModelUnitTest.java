package order;

import burger.model.Burger;
import customer.model.Customer;
import order.exceptions.OrderIncompleteException;
import order.model.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class OrderModelUnitTest {
    @Test
    void instance_AllAttributesAreCorrect_instanceOfOrder(){
        //Arrange
        Integer id = 1;
        List <Burger> myBurgers = new ArrayList<Burger>();
        String description = "Cheese burger without tomatoes";
        Customer myCustomer = Customer.instance(1, "Matias", "Portugal",
                "38480366", "Juan XXIII", "mnportu@gmail.com",
                "3838", LocalDate.of(1995,6,9));
        Burger burger1 = Burger.instance(1,"Piggier", 400.0,
                "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise",5);
        myBurgers.add(burger1);

        //Act
        Order myOrder = Order.instance(myBurgers, description, myCustomer);

        //Assert
        Assertions.assertNotNull(myOrder);
    }
    @Test
    void instance_MustContainAtLeastOneBurger_OrderIncompleteException(){
        //Arrange
        Customer myCustomer = Customer.instance(1, "Matias", "Portugal",
                "38480366", "Juan XXIII", "mnportu@gmail.com",
                "3838", LocalDate.of(1995,6,9));
        List<Burger> myBurgers = new ArrayList<Burger>();
        String description = "Cheese burger without tomatoes";

        //Act
        Exception exceptionEmpty = Assertions.assertThrows(OrderIncompleteException.class,
                ()-> Order.instance(myBurgers, description, myCustomer));

        //Assert
        Assertions.assertEquals("You must select at least one burger", exceptionEmpty.getMessage());
    }

}
