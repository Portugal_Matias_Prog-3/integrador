package burger;

import burger.exceptions.BurgerIncompleteException;
import burger.model.Burger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BurgerModelUnitTest {
    @Test
    void instance_AllAttributesAreCorrect_InstanceOfBurger(){
        //arrange
        Integer id = 1;
        String name = "Piggier";
        double price = 400.0;
        String description = "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise";
        Integer stock = 10;
        //act
        Burger myBurger = Burger.instance(id , name, price, description, stock);
        String myBurgerString = "id: " + id + " - Name: " + name + " - Price:$ " + price +
                " - Description: " + description + " minimum price:$ " + myBurger.MIN_PRICE
                + " - Stock Amount" + stock;
        //assert
        Assertions.assertNotNull(myBurger);
        Assertions.assertTrue(myBurgerString.equals(myBurger.getCompleteInfo()));

    }
    @Test
    void instance_NameIsMissing_BurgerIncompleteException(){
        //arrange
        Integer id = 1;
        String name = "Piggier";
        Double price = 400.0;
        String description = "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise";
        Integer stock = 10;
        //act
        Exception exceptionNull = Assertions.assertThrows(BurgerIncompleteException.class,
                ()-> Burger.instance(id , null, price, description, stock));
        Exception exceptionEmpty = Assertions.assertThrows(BurgerIncompleteException.class,
                ()-> Burger.instance(id , "", price, description, stock));

        //assert
        Assertions.assertEquals("All burgers must have a name" , exceptionNull.getMessage());
        Assertions.assertEquals("All burgers must have a name" , exceptionEmpty.getMessage());
    }
    @Test
    void instance_BelowMinimumPrice_BurgerIncompleteException(){
        //arrange
        Integer id = 1;
        String name = "Piggier";
        Double price = 0.0;
        String description = "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise";
        Integer stock = 10;
        //act
        Exception exceptionMinPrice = Assertions.assertThrows(BurgerIncompleteException.class,
                ()-> Burger.instance(id , name, price, description, stock));

        //assert
        Assertions.assertEquals("Burger price must be over: $" + 350.0 , exceptionMinPrice.getMessage());
    }
    @Test
    void instance_NegativeStockOfBurger_BurgerIncompleteException(){
        //arrange
        Integer id = 1;
        String name = "Piggier";
        Double price = 400.0;
        String description = "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise";
        Integer stock = -5;
        //act
        Exception exceptionNegativeValue = Assertions.assertThrows(BurgerIncompleteException.class,
                ()-> Burger.instance(id , name, price, description, stock));

        //assert
        Assertions.assertEquals("Stock field can't be a negative value", exceptionNegativeValue.getMessage());
    }
    //missing tests are referred to how a burger can't be used if stock is 0, work in progress.
}
