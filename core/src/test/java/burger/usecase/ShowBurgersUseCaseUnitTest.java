package burger.usecase;


import burger.input.ShowBurgersInput;
import burger.interactor.ShowBurgersUseCase;
import burger.model.Burger;
import burger.output.ShowBurgersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ShowBurgersUseCaseUnitTest {
    @Mock
    ShowBurgersRepository showBurgersRepository;

    @Test
    void showsBurgers_BurgersListIsNotEmpty_ShowsBurgers(){
        Collection<Burger> myBurgers = new ArrayList<Burger>();
        Burger myBurger1 = Burger.instance(null, "Piggier", 450.0,
                "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise",10);
        Burger myBurger2 = Burger.instance(null, "Cheese", 500.0,
                "beef meat, 2  cheese slices, tomatoes, mayonnaise",8);
        myBurgers.add(myBurger1);
        myBurgers.add(myBurger2);
        ShowBurgersInput showBurgersInteractor = new ShowBurgersUseCase(showBurgersRepository);

       // when(showBurgersRepository.burgersListIsEmpty()).thenReturn(false);
        when(showBurgersRepository.burgersList()).thenReturn(myBurgers);

        Assertions.assertTrue(myBurgers.equals(showBurgersRepository.burgersList()));

    }
    @Test
    void showsBurgers_BurgersListIsEmpty_BurgersListEmptyException(){
        Collection<Burger> myBurgers = new ArrayList<Burger>();

        ShowBurgersInput showBurgersInteractor = new ShowBurgersUseCase(showBurgersRepository);

         when(showBurgersRepository.burgersListIsEmpty()).thenReturn(true);
        //when(showBurgersRepository.burgersList()).thenReturn(myBurgers);

        Assertions.assertTrue(showBurgersRepository.burgersListIsEmpty());

    }
}
