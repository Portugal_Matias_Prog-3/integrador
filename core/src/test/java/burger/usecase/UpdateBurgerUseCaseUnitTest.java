package burger.usecase;


import burger.exceptions.BurgerFieldChangeException;
import burger.input.UpdateBurgerInput;
import burger.interactor.UpdateBurgerUseCase;
import burger.model.Burger;
import burger.output.UpdateBurgerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UpdateBurgerUseCaseUnitTest {

    @Mock
    UpdateBurgerRepository updateBurgerRepository;
    @Test
    void updateBurger_UpdatePriceAndDescriptionOfBurger_UpdatedBurger(){
        Burger myBurger = Burger.instance(null, "Piggier", 450.0,
                "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise",10);
        String description = "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise, ketchup";
        Double price = 490.0;

        UpdateBurgerInput updateBurgerInteractor = new UpdateBurgerUseCase(updateBurgerRepository);

        when(updateBurgerRepository.getUnmodifiedBurger("Piggier")).thenReturn(myBurger);
        Boolean expected = updateBurgerInteractor.setNewPrice("Piggier",price);
        Boolean expected2 = updateBurgerInteractor.setNewDescription("Piggier",description);

        Assertions.assertTrue(expected);
        Assertions.assertTrue(expected2);

    }
    @Test
    void updateBurger_UpdatePriceOfBurgerUnchanged_BurgerFieldChangeException(){
        Burger myBurger = Burger.instance(null, "Piggier", 450.0,
                "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise",10);
        Double price = 450.0;

        UpdateBurgerInput updateBurgerInteractor = new UpdateBurgerUseCase(updateBurgerRepository);

        when(updateBurgerRepository.getUnmodifiedBurger("Piggier")).thenReturn(myBurger);

        Assertions.assertThrows(BurgerFieldChangeException.class,
                ()->updateBurgerInteractor.setNewPrice("Piggier",price));

    }
    @Test
    void updateBurger_UpdateDescriptionOfBurgerUnchanged_BurgerFieldChangeException(){
        Burger myBurger = Burger.instance(null, "Piggier", 450.0,
                "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise",10);
        String description = "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise";


        UpdateBurgerInput updateBurgerInteractor = new UpdateBurgerUseCase(updateBurgerRepository);

        when(updateBurgerRepository.getUnmodifiedBurger("Piggier")).thenReturn(myBurger);

        Assertions.assertThrows(BurgerFieldChangeException.class,
                ()->updateBurgerInteractor.setNewDescription("Piggier",description));

    }

}
