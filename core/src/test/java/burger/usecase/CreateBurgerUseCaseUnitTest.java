package burger.usecase;

import burger.exceptions.BurgerAlreadyExistsException;
import burger.input.CreateBurgerInput;
import burger.interactor.CreateBurgerUseCase;
import burger.model.Burger;
import burger.output.CreateBurgerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateBurgerUseCaseUnitTest{
    //Create a new burger by name, if already exists don´t
    @Mock
    CreateBurgerRepository createBurgerRepository;

    @Test
    void createsBurger_BurgerDoesNotExist_CreatesBurger(){
        Burger myBurger = Burger.instance(null, "Piggier", 450.0,
                "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise",10);

        CreateBurgerInput createBurgerUseCase = new CreateBurgerUseCase(createBurgerRepository);

        when(createBurgerRepository.existsByName("Piggier")).thenReturn(false);
        when(createBurgerRepository.saveBurger(myBurger)).thenReturn(1);

        Assertions.assertEquals(1,createBurgerUseCase.createBurger(myBurger));
    }
    @Test
    void createBurger_BurgerExist_BurgerAlreadyExistsException(){
        Burger myBurger = Burger.instance(null, "Piggier", 450.0,
                "Pig meat, 2  cheese slices, barbecue sauce, mayonnaise",10);

        verify(createBurgerRepository,never()).saveBurger(myBurger);
        CreateBurgerInput createBurgerUseCase = new CreateBurgerUseCase(createBurgerRepository);

        when(createBurgerRepository.existsByName("Piggier")).thenReturn(true);

        Assertions.assertThrows(BurgerAlreadyExistsException.class,
                ()->createBurgerUseCase.createBurger(myBurger));
    }
}
